# TIRC

Viking Makt IRC Bot

## Settinggs

```json
{
    "channels":["#vm"],
    "pass":"",
    "port":6667,
    "nick":"tirc",
    "rmq":"amqp://localhost",
    "server":"irc.oftc.net",
    "server_pass":false
}
```

* channels -> irc channels
* connect -> connect to irc server
* pass -> nickserv password
* port -> server's port
* nick -> nickname
* rmq -> amqp url
* server -> server's url
* server_pass -> server's password

## Input

```json
utcode
{
  "d":"#vm",
  "m":"some msg"
}
```

* d -> destination, username or channel
* m -> msg

Use **e:tirc_headers{"input":true}** or **e:tirc
rk:tirc.input**. 

## Output

UTCode of irc's payload. Example:

```
:umgeher!~umgeher@191.17.212.195 PRIVMSG #vm :post tirc
```

To receive output's msgs, bind your queue to **e:tirc_headers{"output":true}**.

## changelog

### 0.0.2

* **on_disconnect** method, ioloop stop when disconnect from irc
server
* **connect** settings to irc connection
* using **Announce** with future (rask>=0.0.98)

### 0.0.1

Start of something awesome! 
