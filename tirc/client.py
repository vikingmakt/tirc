from rask.base import Base
from rask.options import options
from socket import socket,AF_INET
from tornado.iostream import IOStream

__all__ = ['Client']

class Client(Base):
    def __init__(self,actions):
        def on_connect():
            self.log.info('connected')

            try:
                assert options.tirc['server_pass']
            except (AssertionError,KeyError):
                pass
            except:
                raise
            else:
                self.write("PASS %s" % options.tirc['server_pass'])

            self.ioengine.loop.add_callback(self.connect)
            return True

        self.actions = actions
        self.stream = IOStream(self.socket_schema)
        self.log.info('connecting')
        self.stream.connect((options.tirc['server'],options.tirc['port']),on_connect)
        self.stream.set_close_callback(self.on_disconnect)
        
    @property
    def socket_schema(self):
        return socket(family=AF_INET)

    def channel_join(self,_):
        self.write("JOIN %s" % _)
        self.pvt("ChanServ","op %s %s" % (_,options.tirc['nick']))
        return True
    
    def channels(self,_=None):
        try:
            c = _.next()
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.channels,
                _=iter(options.tirc['channels'])
            )
        except StopIteration:
            return True
        except:
            pass
        else:
            self.ioengine.loop.add_callback(
                self.channel_join,
                _=c
            )
            self.ioengine.loop.add_callback(
                self.channels,
                _=_
            )
        return None
    
    def identify(self):
        self.write("NICK %s" % options.tirc['nick'])
        self.write("USER %s 0 * :%s" % (options.tirc['nick'],options.tirc['nick']))
        self.pvt("NickServ","identify %s" % options.tirc['pass'])
        return True
    
    def connect(self):
        self.ioengine.loop.add_callback(self.identify)
        self.ioengine.loop.add_callback(self.channels)
        self.stream.read_until(b'\n',self.read_line)
        return True

    def on_disconnect(self,*args):
        self.log.info('Connection close...')
        self.ioengine.loop.stop()
        return True
    
    def pvt(self,d,_):
        return self.write('PRIVMSG %s :%s' % (d,_))

    def read_line(self,_):
        _ = _.rstrip(b'\r\n')
        self.actions(_)
        self.log.info('<<< %s' % _)
        self.stream.read_until(b'\n',self.read_line)
        return True
    
    def write(self,_):
        try:
            assert isinstance(_,str)
        except AssertionError:
            pass
        except:
            raise
        else:
            _ = _.encode('utf-8')

        try:
            self.stream.write(bytes(_ + b'\r\n'))
        except UnicodeEncodeError:
            self.log.info('UnicodeEncodeError: %s' % _)
        except:
            raise
        else:
            self.log.info('output: %s' % _)
        return True
