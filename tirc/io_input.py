from rask.base import Base
from rask.options import options
from rask.rmq import ack

__all__ = ['Input']

class Input(Base):
    def __init__(self,channel,utcode):
        self.channel = channel.result().channel
        self.utcode = utcode
        
        self.channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.tirc['services']['input']['queue']
        )
        
        self.channel.queue_bind(
            arguments={
                'input':True
            },
            callback=None,
            exchange=options.tirc['exchange']['headers'],
            queue=options.tirc['services']['input']['queue'],
            routing_key=''
        )

        self.log.info('started')

    def on_msg(self,channel,method,properties,body):
        def on_decode(_):
            self.ioengine.loop.add_callback(
                self.validate,
                msg=_.result(),
                ack=self.ioengine.future(ack(channel,method))
            )
            return True
        
        self.utcode.decode(body,future=self.ioengine.future(on_decode))
        return True

    def push(self,msg,ack):
        try:
            assert options.tirc['irc']
        except (AssertionError,AttributeError,KeyError):
            ack.set_result(False)
        except:
            raise
        else:
            options.tirc['irc'].pvt(msg['d'],msg['m'])
            ack.set_result(True)
            self.log.info('>>> %s: %s' % (msg['d'],msg['m']))
        return True
    
    def validate(self,msg,ack):
        try:
            assert msg['d']
            assert msg['m']
        except (AssertionError,AttributeError):
            self.log.info('invalid payload')
            ack.set_result(True)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.push,
                msg=msg,
                ack=ack
            )
        return True
            
            
