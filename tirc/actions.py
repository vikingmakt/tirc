from rask.base import Base
from rask.options import options

__all__ = ['Actions']

class Actions(Base):
    def parser(self,_):
        try:
            assert _.startswith('PING :')
        except AssertionError:
            self.ioengine.loop.add_callback(
                options.tirc['output'],
                msg=_
            )
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.pong,
                _=_
            )
        return True

    def pong(self,_):
        _ = _.replace('PING','PONG')
        options.tirc['irc'].write(_)
        self.log.info('PING/PONG')
        return True
