from rask.base import Base
from rask.options import options
from rask.rmq import BasicProperties

__all__ = ['Output']

class Output(Base):
    def __init__(self,channel,utcode):
        self.channel = channel.result().channel
        self.utcode = utcode

    def push(self,msg):
        def on_encode(_):
            self.channel.basic_publish(
                body=_.result(),
                exchange=options.tirc['exchange']['topic'],
                properties=BasicProperties(headers={
                    'output':True
                }),
                routing_key=''
            )
            return True

        self.utcode.encode(unicode(msg),future=self.ioengine.future(on_encode))
        return True
