from actions import Actions
from client import Client
from io_input import Input
from output import Output
from rask.main import Main
from rask.options import define,options
from rask.parser.json import dictfy
from rask.parser.utcode import UTCode
from rask.rmq import Announce,RMQ

__all__ = ['Run']

class Run(Main):
    __settings = {
        'exchange':{
            'topic':'tirc',
            'headers':'tirc_headers'
        },
        'services':{
            'input':{
                'queue':'tirc_input',
                'rk':'tirc.input'
            }
        }
    }

    @property
    def actions(self):
        try:
            assert self.__actions
        except (AssertionError,AttributeError):
            self.__actions = Actions().parser
        except:
            raise
        return self.__actions

    @property
    def rmq(self):
        try:
            assert self.__rmq
        except (AssertionError,AttributeError):
            self.__rmq = RMQ(url=options.tirc['rmq'])
        except:
            raise
        return self.__rmq

    @property
    def utcode(self):
        try:
            assert self.__utcode
        except (AssertionError,AttributeError):
            self.__utcode = UTCode()
        except:
            raise
        return self.__utcode
    
    def after(self):
        self.ioengine.loop.add_callback(self.settings_load)
        return True

    def before(self):
        self.log.info('TIRC')
        return True

    def connect(self):
        try:
            assert options.tirc['connect']
        except AssertionError:
            pass
        except:
            raise
        else:
            options.tirc['irc'] = Client(actions=self.actions)
        return True
    
    def services_init(self,*args):
        def on_input(_):
            options.tirc['input'] = Input(channel=_,utcode=self.utcode)
            return True

        def on_output(_):
            options.tirc['output'] = Output(channel=_,utcode=self.utcode).push
            return True

        self.rmq.channel('input',future=self.ioengine.future(on_input))
        self.rmq.channel('output',future=self.ioengine.future(on_output))
        self.ioengine.loop.add_callback(
            self.connect
        )
        return True
    
    def settings_check(self,_=None):
        try:
            assert _.next() in options.tirc
        except AssertionError:
            self.log.error('invalid settings!')
            self.ioengine.stop()
            return False
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.settings_check,
                _=iter([
                    'channels',
                    'connect',
                    'pass',
                    'port',
                    'nick',
                    'rmq',
                    'server',
                    'server_pass'
                ])
            )
        except StopIteration:
            def on_announce(_):
                Announce(
                    channel=_,
                    settings=options.tirc,
                    future=self.ioengine.future(self.services_init)
                )
                return True
            
            self.rmq.channel('announce',future=self.ioengine.future(on_announce))
            return True
        except:
            raise
        else:
            self.ioengine.loop.add_callback(self.settings_check,_)
        return None

    def settings_load(self):
        try:
            rfile = open('settings.json', 'r')
            rconf = dictfy(rfile.read())
            rfile.close()
            assert rconf
        except AssertionError:
            self.log.error('invalid settings json!')
            self.ioengine.stop()
            return False
        except IOError:
            self.log.error('no settings file found!')
        except:
            raise
        else:
            self.__settings.update(rconf)

        define('tirc',default=self.__settings)
        self.ioengine.loop.add_callback(self.settings_check)
        return True
